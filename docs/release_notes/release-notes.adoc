= JIRA Rest Connector Release Notes - Mule 4

== Jira Rest Connector 1.0.0 - ***Date Of Release TBD***

Release Notes for version V1.0.0 of the JIRA Rest Connector. These Release Notes accompany the link:https://hotovo.gitlab.io/mule-connectors/mule4-jira-rest-connector/docs/jira-rest-connector-reference.html[Technical Reference] guide.
 
=== Version 1.0.0 - Compatibility
The JIRA Rest connector is compatible with:

|===
|Application/Service|Version

|Mule Runtime|4.1.1 EE or later
|JIRA Cloud Rest API| 6.x
|===

=== Version 1.0.0 - Features
* N/A

=== Version 1.0.0 - Fixed in this Release
* N/A

=== Version 1.0.0 - Known Issues
* When using OAuth2 authentication with older Mule runtime than *4.2.2*, connector is unable to reconnect after the access token expires.

=== Support Resources

* JIRA Rest Connector example can be found at link:https://gitlab.com/hotovo/mule-connectors/tree/master/mule4-jira-rest-connector/jira-crud-demo[CRUD demo application]. Demo explaining OAuth authentication process
link:https://gitlab.com/hotovo/mule-connectors/tree/master/mule4-jira-rest-connector/jira-oauth-demo[OAuth demo application].
* Learn how to link:https://docs.mulesoft.com/mule-runtime/3.9/installing-connectors[Install Anypoint Connectors] using Anypoint Exchange.
* Access the link:http://forum.mulesoft.org/mulesoft[MuleSoft Forum] to pose questions and get help from Mule’s broad community of users.
* To access MuleSoft’s expert support team, link:http://www.mulesoft.com/mule-esb-subscription[subscribe] and log in to MuleSoft’s link:http://www.mulesoft.com/support-login[Customer Portal].