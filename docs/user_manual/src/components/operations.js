import React from "react";
import Layout from "./layout";
import Card from "./card";
import "./operations.css";

const Operations = () => {
  return (
    <Layout>
      <Card>
        <h2>Operations</h2>
        <div className="operations-container">
          <ul className="ul-list">
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Attachments</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Components</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Issues</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Issue Comments</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Issue Links</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Issue Link Types</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Issue Priorities</span>
            </li>
          </ul>
          <ul className="ul-list">
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Issue Remote Links</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Issue Types</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Issue Votes</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Issue Watchers</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Projects</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Project Categories</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Statuses</span>
            </li>
          </ul>
          <ul className="ul-list">
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Users</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>User Groups</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Versions</span>
            </li>
            <li>
              CRUD operations for{" "}
              <span style={{ fontWeight: "bold" }}>Worklogs</span>
            </li>
            <li>JQL Search for Issues</li>
            <li>Get Current logged User</li>
            <li>OAuth authorization operations</li>
          </ul>
        </div>
        <div className="tip">
          <p className="tip-title">NOTE</p>
          <p className="tip-description">
            <div>
              See a full list of operations in the{" "}
              <a
                target="_blank"
                rel="noopener noreferrer"
                className="anchor"
                href="https://hotovo.gitlab.io/mule-connectors/mule4-jira-rest-connector/docs/jira-rest-connector-reference.html"
              >
                ApiDocs
              </a>
              .{" "}
            </div>
          </p>
        </div>
      </Card>
      <Card>
        <h2>Common Use Cases</h2>
        <p style={{ marginBottom: "16px" }}>
          Explain the common and less intuitive use cases and provide links to
          them in the bullets.
        </p>
        <ul className="ul-list">
          <li>Inbound Scenario</li>
          <li>Outbound Scenario</li>
        </ul>
        <h3>Inbound Scenario</h3>
        <p>
          Use the{" "}
          <a
            target="_blank"
            rel="noopener noreferrer"
            className="anchor"
            href="https://docs.mulesoft.com/mule-runtime/4.2/scheduler-concept"
          >
            Scheduler Scope
          </a>{" "}
          as a Source at the beginning of the flow, place a Jira Rest component
          to the execution part of this flow and schedule polling the data from
          Jira into your Mule application.
        </p>
        <div className="image-container">
          <img src={require("../images/inbound.png")} alt="Inbound" />
        </div>
        <ol className="ol-list">
          <li>
            <span style={{ fontWeight: "bold" }}>Scheduler</span> - Triggers the
            flow when on scheduled time.
          </li>
          <li>
            <span style={{ fontWeight: "bold" }}>Jira Rest Connector</span> -
            retrieves data from Jira.
          </li>
          <li>
            <span style={{ fontWeight: "bold" }}>For Each Scope</span> - Splits
            retrieved collection into individual records.
          </li>
          <li>
            <span style={{ fontWeight: "bold" }}>Logger</span> - Logs details
            about each record.
          </li>
          <li>
            <span style={{ fontWeight: "bold" }}>Transformer</span> - Transforms
            each project json into POJO
          </li>
          <li>
            <span style={{ fontWeight: "bold" }}>Insert</span> - Inserts each
            project into the database
          </li>
        </ol>
        <h3>Outbound Scenario</h3>
        <p>
          Use as an outbound connector in your flow to push data into Jira. To
          use the connector in this manner, simply place the connector in your
          flow at any point after an inbound endpoint.
        </p>
        <div className="image-container">
          <img src={require("../images/outbound.png")} alt="Outbound" />
        </div>
        <ol className="ol-list">
          <li>
            <span style={{ fontWeight: "bold" }}>HTTP Listener Endpoint</span> -
            listens for HTTP requests.
          </li>
          <li>
            <span style={{ fontWeight: "bold" }}>Transform Message</span> -
            transforms incoming message to form suitable for following Jira Rest
            connector.
          </li>
          <li>
            <span style={{ fontWeight: "bold" }}>Jira Rest Connector</span> -
            connects to Jira and performs selected operation using received
            data.
          </li>
          <li>
            <span style={{ fontWeight: "bold" }}>Logger</span> - Logs details
            about each record.
          </li>
        </ol>
      </Card>
      <Card>
        <h2>Connector Performance</h2>
        <p>
          Mule 4 uses repeatable streams as its default framework for handling
          streams. Repeatable streams enable you to read a stream more than once
          and have concurrent access to the stream.
        </p>
        <p>
          You can improve the connector perfomance by choosing and configuring
          the best{" "}
          <a
            className="anchor"
            href="https://docs.mulesoft.com/mule-runtime/4.2/streaming-about#streaming-strategies"
          >
            Streaming Strategy
          </a>{" "}
          which suits your requierements.
        </p>
        <p>
          For background information on pooling, see{" "}
          <a
            className="anchor"
            href="https://docs.mulesoft.com/mule-runtime/4.2/streaming-about"
          >
            Streaming
          </a>
          .
        </p>
      </Card>
      <Card>
        <h2>Resources</h2>
        <p>
          Access the{" "}
          <a
            target="_blank"
            rel="noopener noreferrer"
            className="anchor"
            href="https://hotovo.gitlab.io/mule-connectors/mule4-jira-rest-connector/docs/release-notes.html"
          >
            JIRA Rest Connector Release Notes.
          </a>
        </p>
      </Card>
    </Layout>
  );
};

export default Operations;
