import React from "react";
import { Link } from "react-router-dom";
import "./sidenav.css";

const Sidenav = () => {
  return (
    <aside className="sidenav">
      <ul className="sidenav__list">
        <li className="sidenav__list-item">
          <Link className="link" to="/">
            JIRA Rest Connector
          </Link>
        </li>
        <li className="sidenav__list-item">
          <Link className="link" to="/prerequisites">
            Prerequisites
          </Link>
        </li>
        <li className="sidenav__list-item">
          <Link className="link" to="/configure">
            How to Configure
          </Link>
        </li>
        <li className="sidenav__list-item">
          <Link className="link" to="/operations">
            Operations
          </Link>
        </li>
      </ul>
    </aside>
  );
};

export default Sidenav;
