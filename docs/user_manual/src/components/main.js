import React from "react";
import Layout from "./layout";
import Card from "./card";
import "./main.css";

const Main = () => {
  return (
    <Layout>
      <Card>
        <h4>JIRA Rest Connector</h4>
        <p>
          Integrate JIRA with existing SaaS and on-premises applications quickly
          and easily using the Atlassian JIRA Rest connector. This connector
          supports Basic authentication or OAuth2 authentication and it provides
          execution of operations in a{" "}
          <a
            href="https://docs.mulesoft.com/mule-sdk/1.1/non-blocking-operations"
            className="anchor"
            target="_blank"
            rel="noopener noreferrer"
          >
            Non-Blocking manner
          </a>
          . Using the connector, your application can perform up to 80
          operations which JIRA exposes via their REST API.
        </p>
        <p>
          Read through this user guide to understand how to set up and configure
          a basic flow using the connector. Track feature additions,
          compatibility, limitations and API version updates with each release
          of the connector using the{" "}
          <a
            className="anchor"
            href="https://hotovo.gitlab.io/mule4-jira-rest-connector/docs/release_notes"
            target="_blank"
            rel="noopener noreferrer"
          >
            Connector Release Notes
          </a>
          . Review the connector operations and functionality using the{" "}
          <a
            className="anchor"
            href="https://hotovo.gitlab.io/mule4-jira-rest-connector/docs/connector_reference"
            target="_blank"
            rel="noopener noreferrer"
          >
            Technical Reference
          </a>{" "}
          alongside the{" "}
          <a
            href="https://gitlab.com/hotovo/mule4-jira-rest-connector/tree/master/jira-crud-demo"
            className="anchor"
            target="_blank"
            rel="noopener noreferrer"
          >
            demo application
          </a>
          .
        </p>
        <p>
          MuleSoft maintains this connector under the{" "}
          <a
            className="anchor"
            href="https://docs.mulesoft.com/mule-runtime/3.8/anypoint-connectors#connector-categories"
            target="_blank"
            rel="noopener noreferrer"
          >
            MuleSoft Certified Category
          </a>{" "}
          support policy.
        </p>
      </Card>
    </Layout>
  );
};

export default Main;
